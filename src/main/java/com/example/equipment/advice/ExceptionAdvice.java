package com.example.equipment.advice;

import com.example.equipment.enums.ResultCode;
import com.example.equipment.exception.CMissingDateException;
import com.example.equipment.exception.CNoEmployeeDateException;
import com.example.equipment.model.CommonResult;
import com.example.equipment.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    //protected 보호되어있다.

    @ExceptionHandler(Exception.class)  //
    @ResponseStatus(HttpStatus.BAD_REQUEST)
   //defaultException 예상치 못한 일이 발생하면 기본익셉션으로 간다.(디폴트익셉션)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
       return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDateException e){
        return ResponseService.getFailResult(ResultCode.MISSING_DATE);
    }
    @ExceptionHandler(CNoEmployeeDateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoEmployeeDateException e){
        return ResponseService.getFailResult(ResultCode.No_EMPLOYEE_DATE);
    }
}
