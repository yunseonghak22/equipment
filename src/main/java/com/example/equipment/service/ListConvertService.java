package com.example.equipment.service;


import com.example.equipment.model.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService {
    public static PageRequest getPageable(int pageNum){   // 페이지를 나누는 메소드
        return PageRequest.of(pageNum - 1, 10);  //pageNum 한페이지당 10개

    }
    public static PageRequest getPageable(int pageNum, int pageSize){  //pageSize은 멀까
        return PageRequest.of(pageNum - 1, pageSize);
    }
    public static <T>ListResult<T> settingResult(List<T> List){   // 통으로 가져온다.
        ListResult<T> result =  new ListResult<>();   //ListResult 필드 을 만든다.
        result.setList(List);
        result.setTotalItemCount((long)List.size());   // List.size가 멀까?
        result.setTotalPage(1);   //뭉뎅이 받아서 전체페이지 1
        result.setCurrentPage(1); //뭉뎅이 받아서 현재페이지 1

        return result;
    }
    //페이지를 나눠지는 부분
    public static <T> ListResult<T> settingResult(List<T> list, long totalItemCount,int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount(totalItemCount);
        result.setTotalPage(totalPage == 0 ? 1 : totalPage);
        //조건식 ? 식1 : 식2==> 조건식이 true 라면 식1, 조건식이 false라면 식2
        result.setCurrentPage(currentPage + 1);

        return  result;
    }
}
