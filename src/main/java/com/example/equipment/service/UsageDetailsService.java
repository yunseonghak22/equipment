package com.example.equipment.service;

import com.example.equipment.entity.Employee;
import com.example.equipment.entity.Equipment;
import com.example.equipment.entity.UsageDetails;
import com.example.equipment.model.ListResult;
import com.example.equipment.model.UsageDetailsItem;
import com.example.equipment.repository.UsageDetailsRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {
    private final UsageDetailsRepository usageDetailsRepository;

    public void setUsageDetails(Equipment equipment, Employee employee, LocalDateTime dateUsage){
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(equipment,employee,dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }
    public ListResult<UsageDetailsItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd){
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
    List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStart,dateEnd);
    List<UsageDetailsItem> result = new LinkedList<>();
    usageDetails.forEach(usageDetails1 -> {
        UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetails1).build();
        result.add(addItem);
    });
    return ListConvertService.settingResult(result);
    }
}
