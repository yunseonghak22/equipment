package com.example.equipment.service;

import com.example.equipment.entity.Equipment;
import com.example.equipment.enums.EquipmentType;
import com.example.equipment.exception.CMissingDateException;
import com.example.equipment.model.*;
import com.example.equipment.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    public Equipment getEquipmentId(long id){
        return equipmentRepository.findById(id).orElseThrow(CMissingDateException::new);
    }
    public void setEquipment(EquipmentRequest request){
        Equipment equipment = new Equipment.EquipmentBuilder(request).build();
        equipmentRepository.save(equipment);
    }
    public EquipmentDetail getEquipment(long id){
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDateException::new);
        EquipmentDetail result = new EquipmentDetail.EquipmentDetailBuilder(equipment).build();
        return result;
    }
    public ListResult<EquipmentItem> getEquipments(){
        List<Equipment> equipments = equipmentRepository.findAll();
        List<EquipmentItem> result = new LinkedList<>();
        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
    public ListResult<EquipmentItem> getEquipments(EquipmentType equipmentType){
        List<Equipment> equipments = equipmentRepository.findAllByEquipmentTypeOrderByIdDesc(equipmentType);
        List<EquipmentItem> result = new LinkedList<>();
        equipments.forEach(equipment -> {
            EquipmentItem addItem = new EquipmentItem.EquipmentItemBuilder(equipment).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
    public void putEquipmentUpdate(long id, EquipmentDiscardRequest request){
        Equipment equipment = equipmentRepository.findById(id).orElseThrow(CMissingDateException::new);
        equipment.putEquipmentDiscard(request);
        equipmentRepository.save(equipment);
    }
}
