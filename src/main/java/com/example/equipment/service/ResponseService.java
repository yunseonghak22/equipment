package com.example.equipment.service;

import com.example.equipment.enums.ResultCode;
import com.example.equipment.model.CommonResult;
import com.example.equipment.model.ListResult;
import com.example.equipment.model.SingleResult;
import org.springframework.stereotype.Service;

@Service  //협업하는 애가 없어서 Rear을 안달았다.
public class ResponseService {    //static 엄청 자주 사용하는 곳에 쓴인다.
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess){
        if (isSuccess) setSuccessResult(result);
        else setFailResult(result);
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T date){
       SingleResult<T> result = new SingleResult<>();
       result.setData(date);    //모델 singResult 등록
       setSuccessResult(result); // 모델 singResult 상속 밑에 private 있다.
       return result;
    }
    //
    public static CommonResult getSuccessResult(){   //성공결과을 가져온다.
        CommonResult result = new CommonResult();
        setSuccessResult(result);
        return result;
    }
    //성공 유형은 하나고 실패 유형은 여러개다.

    public static CommonResult getFailResult(ResultCode resultCode) { //어떤 유형의 페일인지 받아야한다

        CommonResult result = new CommonResult();
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());
        return result;

    }
    //성공결과을 등록한다.( commonResult
    private static void setSuccessResult(CommonResult result){
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }
    private static void setFailResult(CommonResult result){
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }
}
