package com.example.equipment.service;

import com.example.equipment.entity.Employee;
import com.example.equipment.exception.CMissingDateException;
import com.example.equipment.exception.CNoEmployeeDateException;
import com.example.equipment.model.EmployeeItem;
import com.example.equipment.model.EmployeeRequest;
import com.example.equipment.model.ListResult;
import com.example.equipment.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public  Employee getEmployeeId(long id){
         return  employeeRepository.findById(id).orElseThrow(CMissingDateException::new);
    }
    public void setEmployee(EmployeeRequest request){
        Employee employee = new Employee.EmployeeBuilder(request).build();
        employeeRepository.save(employee);
    }
    public EmployeeItem getEmployee(long id){
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDateException::new);
        EmployeeItem result = new EmployeeItem.EmployeeItemBuilder(employee).build();
        return result;
    }
    public ListResult<EmployeeItem> getEmployees(){
        List<Employee> employeeItems = employeeRepository.findAll();
        List<EmployeeItem> result = new LinkedList<>();
        employeeItems.forEach(employee -> {
            EmployeeItem addItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
    public ListResult<EmployeeItem> getEmployees(boolean isEnable){
        List<Employee> employees = employeeRepository.findAllByIsEnableOrderByIdDesc(isEnable);
        List<EmployeeItem> result = new LinkedList<>();
        employees.forEach(employee -> {
            EmployeeItem addItem = new EmployeeItem.EmployeeItemBuilder(employee).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }
    public void putEmployee(long id){
        Employee employee = employeeRepository.findById(id).orElseThrow(CMissingDateException::new);

        if(!employee.getIsEnable()) throw new CNoEmployeeDateException();
        employee.putResign();
        employeeRepository.save(employee);
    }

}
