package com.example.equipment.entity;

import com.example.equipment.interfaces.CommonModelBuilder;
import com.example.equipment.model.EmployeeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String employeeNum;

    @Column(nullable = false, length = 20)
    private String employeeName;

    @Column(nullable = false, length = 20)
    private String employeeTel;

    @Column(nullable = false, length = 30)
    private String employeeAddress;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false)
    private LocalDateTime dateJoin;

    private LocalDateTime dateResign;
    public void putResign(){
        this.isEnable = false;
        this.dateResign = LocalDateTime.now();
    }
    private Employee(EmployeeBuilder builder){
        this.employeeNum = builder.employeeNum;
        this.employeeName = builder.employeeName;
        this.employeeTel = builder.employeeTel;
        this.employeeAddress =builder.employeeAddress;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateResign = builder.dateResign;
    }

    public static class EmployeeBuilder implements CommonModelBuilder<Employee>{
        private final String employeeNum;
        private final String employeeName;
        private final String employeeTel;
        private final String employeeAddress;
        private final Boolean isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateResign;
        public EmployeeBuilder(EmployeeRequest request){
            this.employeeNum = request.getEmployeeNum();
            this.employeeName =request.getEmployeeName();
            this.employeeTel =request.getEmployeeTel();
            this.employeeAddress = request.getEmployeeAddress();
            this.isEnable = true;
            this.dateJoin = LocalDateTime.now();
            this.dateResign = LocalDateTime.now();
        }
        @Override
        public Employee build() {
            return new Employee(this);
        }
    }
}
