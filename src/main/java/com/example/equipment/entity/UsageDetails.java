package com.example.equipment.entity;

import com.example.equipment.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipmentId", nullable = false)
    private Equipment equipment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId", nullable = false)
    private Employee employee;

    @Column(nullable = false)
    private LocalDateTime dateUsage;
    private UsageDetails(UsageDetailsBuilder builder){
        this.equipment = builder.equipment;
        this.employee = builder.employee;
        this.dateUsage = builder.dateUsage;
    }
    public static class UsageDetailsBuilder implements CommonModelBuilder<UsageDetails>{
        private final Equipment equipment;
        private final Employee employee;
        private final LocalDateTime dateUsage;
        public UsageDetailsBuilder(Equipment equipment,Employee employee, LocalDateTime dateUsage){
            this.equipment = equipment;
            this.employee = employee;
            this.dateUsage = dateUsage;
        }
        @Override
        public UsageDetails build() {
            return new UsageDetails(this);
        }
    }
}
