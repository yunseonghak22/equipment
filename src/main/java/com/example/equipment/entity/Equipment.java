package com.example.equipment.entity;

import com.example.equipment.enums.EquipmentType;
import com.example.equipment.interfaces.CommonModelBuilder;
import com.example.equipment.model.EquipmentDiscardRequest;
import com.example.equipment.model.EquipmentRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private EquipmentType equipmentType;

    @Column(nullable = false, length = 20)
    private String manufacturer;

    @Column(nullable = false, length = 20)
    private String productModel;

    @Column(nullable = false)
    private Integer productQuantity;

    @Column(nullable = false)
    private LocalDate datePurchase;

    @Column(nullable = false)
    private Double purchasePrice;

    @Column(nullable = true)
    private LocalDate productDiscard;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putEquipmentDiscard(EquipmentDiscardRequest request){
        this.productDiscard = request.getProductDiscard();
        this.dateUpdate = LocalDateTime.now();
    }
        private Equipment(EquipmentBuilder builder){
            this.equipmentType = builder.equipmentType;
            this.manufacturer = builder.manufacturer;
            this.productModel = builder.productModel;
            this.productQuantity = builder.productQuantity;
            this.datePurchase = builder.datePurchase;
            this.purchasePrice = builder.purchasePrice;
            this.productDiscard = builder.productDiscard;
            this.dateCreate = builder.dateCreate;
            this.dateUpdate = builder.dateUpdate;

        }
    public static class EquipmentBuilder implements CommonModelBuilder<Equipment>{
        private final EquipmentType equipmentType;
        private final String manufacturer;
        private final String productModel;
        private final Integer productQuantity;
        private final LocalDate datePurchase;
        private final Double purchasePrice;
        private final LocalDate productDiscard;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        public EquipmentBuilder(EquipmentRequest request){
            this.equipmentType = request.getEquipmentType();
            this.manufacturer = request.getManufacturer();
            this.productModel = request.getProductModel();
            this.productQuantity = request.getProductQuality();
            this.datePurchase = request.getDatePurchase();
            this.purchasePrice = request.getPurchasePrice();
            this.productDiscard = request.getProductDiscard();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }
        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }

}
