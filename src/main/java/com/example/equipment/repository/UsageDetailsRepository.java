package com.example.equipment.repository;

import com.example.equipment.entity.UsageDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface UsageDetailsRepository extends JpaRepository<UsageDetails,Long> {
    List<UsageDetails> findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(LocalDate dateStart, LocalDate DateEnd);
}
