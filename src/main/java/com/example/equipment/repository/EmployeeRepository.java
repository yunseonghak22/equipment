package com.example.equipment.repository;

import com.example.equipment.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    List<Employee> findAllByIsEnableOrderByIdDesc(Boolean isEnable);
}
