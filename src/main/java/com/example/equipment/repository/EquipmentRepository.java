package com.example.equipment.repository;

import com.example.equipment.entity.Equipment;
import com.example.equipment.enums.EquipmentType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    List<Equipment> findAllByEquipmentTypeOrderByIdDesc(EquipmentType equipmentType);
}
