package com.example.equipment.controller;

import com.example.equipment.model.*;
import com.example.equipment.service.EmployeeService;
import com.example.equipment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "사원api")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @ApiOperation(value = "사원등록")
    @PostMapping("/new")
    public CommonResult setEmployee(@RequestBody @Valid EmployeeRequest request){
        employeeService.setEmployee(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사원한명조회")
    @GetMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "사원번호시퀀스",required = true)
    })
    public SingleResult<EmployeeItem> getEmployee(@PathVariable long id){
        return ResponseService.getSingleResult(employeeService.getEmployee(id));
    }

    @ApiOperation(value = "사원전체조회")
    @GetMapping("/search")
    public ListResult<EmployeeItem> getEmployee(){
        return ResponseService.getListResult(employeeService.getEmployees(),true);
    }

    @ApiOperation(value = "사원삭제")
    @DeleteMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사원번호시퀀스",required = true)
    })
    public CommonResult delEmployee(@PathVariable long id){
        employeeService.putEmployee(id);
        return ResponseService.getSuccessResult();
    }

}
