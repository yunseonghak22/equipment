package com.example.equipment.controller;


import com.example.equipment.entity.Employee;
import com.example.equipment.entity.Equipment;
import com.example.equipment.model.CommonResult;
import com.example.equipment.model.ListResult;
import com.example.equipment.model.UsageDetailsItem;
import com.example.equipment.model.UsageDetailsRequest;
import com.example.equipment.service.EmployeeService;
import com.example.equipment.service.EquipmentService;
import com.example.equipment.service.ResponseService;
import com.example.equipment.service.UsageDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "사용내역Api")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")
public class UsageDetailsController {
    private final UsageDetailsService usageDetailsService;
    private final EquipmentService equipmentService;
    private final EmployeeService employeeService;

    @ApiOperation(value = "사용내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest request){
        Equipment equipment = equipmentService.getEquipmentId(request.getEquipmentId());
        Employee employee =employeeService.getEmployeeId(request.getEmployeeId());
        usageDetailsService.setUsageDetails(equipment,employee,request.getDateUsage());
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사용내역 조회")
    @GetMapping("/search")
    public ListResult<UsageDetailsItem> getUsageDetails(
            @RequestParam(value = "dateStart",required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateEnd
    ){
     return ResponseService.getListResult(usageDetailsService.getUsageDetails(dateStart, dateEnd),true);
    }
}
