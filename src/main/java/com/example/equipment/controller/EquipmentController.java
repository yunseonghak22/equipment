package com.example.equipment.controller;

import com.example.equipment.enums.EquipmentType;
import com.example.equipment.model.*;
import com.example.equipment.service.EquipmentService;
import com.example.equipment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기자재등록")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/equipment")
public class EquipmentController {
    private final EquipmentService equipmentService;

    @ApiOperation(value = "기자재등록")
    @PostMapping("/new")
    public CommonResult setEquipment(@RequestBody @Valid EquipmentRequest request){
        equipmentService.setEquipment(request);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기자재번호시퀀스",required = true)
    })
    public SingleResult<EquipmentDetail> getEquipment(@PathVariable long id){
        return ResponseService.getSingleResult(equipmentService.getEquipment(id));
    }
    @ApiOperation(value = "기자재전체조회")
    @GetMapping("/search")
    public ListResult<EquipmentItem> getEquipments(@RequestParam(name = "equipmentType",required = false)EquipmentType equipmentType){
        if(equipmentType == null){
            return ResponseService.getListResult(equipmentService.getEquipments(),true);
        }else {
            return ResponseService.getListResult((equipmentService.getEquipments(equipmentType)),true);
        }
    }

    @ApiOperation(value = "기자재 수정")
    @PutMapping("/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "기자재번호시퀀스", required = true)
    })
    public CommonResult putEquipment(@PathVariable long id, @RequestBody @Valid EquipmentDiscardRequest request){
        equipmentService.putEquipmentUpdate(id, request);
        return ResponseService.getSuccessResult();
    }
}
