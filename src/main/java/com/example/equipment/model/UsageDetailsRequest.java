package com.example.equipment.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Setter
@Getter
public class UsageDetailsRequest {
    @ApiModelProperty(notes = "기자재id")
    @NotNull
    private Long equipmentId;

    @ApiModelProperty(notes = "사원번호id")
    @NotNull
    private Long employeeId;

    @ApiModelProperty(notes = "기자재사용내역")
    @NotNull
    private LocalDateTime dateUsage;
}
