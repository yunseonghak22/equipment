package com.example.equipment.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ListResult<T> extends CommonResult{

    private List<T> list;
    private Long totalItemCount; //총 아이템수개 나태내는 변수
    private Integer totalPage;  // 총 페이지 수
    private Integer currentPage; // 몇번째 페이지 인지?
}
