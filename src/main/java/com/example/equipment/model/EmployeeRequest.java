package com.example.equipment.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EmployeeRequest {

    @ApiModelProperty(notes = "사원번호")
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeNum;

    @ApiModelProperty(notes = "사원이름")
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeName;

    @ApiModelProperty(notes = "사원전화번호")
    @NotNull
    @Length(min = 2, max = 20)
    private String employeeTel;

    @ApiModelProperty(notes = "사원주소")
    @NotNull
    @Length(min = 2, max = 30)
    private String employeeAddress;
}
