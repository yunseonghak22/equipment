package com.example.equipment.model;

import com.example.equipment.enums.EquipmentType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Setter
@Getter
public class EquipmentRequest {
    @ApiModelProperty(notes = "기자재종류")
    @Enumerated(value = EnumType.STRING)
    private EquipmentType equipmentType;

    @ApiModelProperty(notes = "제조사")
    @NotNull
    @Length(min = 2, max = 20)
    private String manufacturer;

    @ApiModelProperty(notes = "기자재모델")
    @NotNull
    @Length(min=2, max = 20)
    private String productModel;

    @ApiModelProperty(notes = "기자재수량")
    @NotNull
    private Integer productQuality;

    @ApiModelProperty(notes = "기자재구매일")
    @NotNull
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "기자재구매일")
    @NotNull
    private Double purchasePrice;

    @ApiModelProperty(notes = "기자재폐기일")
    private LocalDate productDiscard;
}
