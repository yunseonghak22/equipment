package com.example.equipment.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Setter
@Getter
public class EquipmentDiscardRequest {
    @ApiModelProperty(notes = "기자재폐기일")
    private LocalDate productDiscard;
}
