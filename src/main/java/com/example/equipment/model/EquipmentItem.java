package com.example.equipment.model;

import com.example.equipment.entity.Equipment;
import com.example.equipment.enums.EquipmentType;
import com.example.equipment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {
    @ApiModelProperty(notes = "기자재시퀀스")
    private Long id;
    @ApiModelProperty(notes = "기자재풀이름")
    private String equipmentFullName;
    @ApiModelProperty(notes = "기자재모델")
    private String productModel;
    @ApiModelProperty(notes = "기자재수량")
    private String productQuantity;
    @ApiModelProperty(notes = "기자재구매일")
    private LocalDate datePurchase;
    @ApiModelProperty(notes = "기자재가격")
    private String purchasePrice;
    @ApiModelProperty(notes = "기자재생성일")
    private LocalDateTime dateCreate;
    @ApiModelProperty(notes = "기자재업데이트일")
    private LocalDateTime dateUpdate;
    private EquipmentItem(EquipmentItemBuilder builder){
        this.id = builder.id;
        this.equipmentFullName = builder.equipmentFullName;
        this.productModel = builder.productModel;
        this.productQuantity = builder.productQuantity;
    }
    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem>{
        private final Long id;
        private final String equipmentFullName;
        private final String productModel;
        private final String productQuantity;

        public EquipmentItemBuilder(Equipment equipment){
            this.id =equipment.getId();
            this.equipmentFullName = equipment.getManufacturer() + " " +equipment.getEquipmentType();
            this.productModel = equipment.getProductModel();
            this.productQuantity = equipment.getProductQuantity()+ "개";

        }
        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }
}
