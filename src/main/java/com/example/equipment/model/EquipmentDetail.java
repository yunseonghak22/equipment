package com.example.equipment.model;

import com.example.equipment.entity.Equipment;
import com.example.equipment.enums.EquipmentType;
import com.example.equipment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentDetail {
    @ApiModelProperty(notes = "기자재시퀀스")
    private Long id;
    @ApiModelProperty(notes = "기자재풀이름")
    private String equipmentFullName;
    @ApiModelProperty(notes = "기자재모델")
    private String productModel;
    @ApiModelProperty(notes = "기자재수량")
    private String productQuantity;
    @ApiModelProperty(notes = "기자재구매일")
    private LocalDate datePurchase;
        private EquipmentDetail(EquipmentDetailBuilder builder){
            this.id = builder.id;
            this.equipmentFullName = builder.equipmentFullName;
            this.productModel = builder.productModel;
            this.productQuantity = builder.productQuantity;
            this.datePurchase = builder.datePurchase;
        }
    public static class EquipmentDetailBuilder implements CommonModelBuilder<EquipmentDetail>{
        private final Long id;
        private final String equipmentFullName;
        private final String productModel;
        private final String productQuantity;
        private final LocalDate datePurchase;
        public EquipmentDetailBuilder(Equipment equipment){
            this.id=equipment.getId();
            this.equipmentFullName = equipment.getEquipmentType()+" " + equipment.getManufacturer();
            this.productModel = equipment.getProductModel();
            this.productQuantity =equipment.getProductQuantity()+ "개";
            this.datePurchase = equipment.getDatePurchase();
        }
        @Override
        public EquipmentDetail build() {
            return new EquipmentDetail(this);
        }
    }


}
