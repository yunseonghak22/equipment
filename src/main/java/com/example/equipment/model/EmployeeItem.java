package com.example.equipment.model;

import com.example.equipment.entity.Employee;
import com.example.equipment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EmployeeItem {
    private Long id;

    private String employeeNum;

    private String employeeName;

    private String employeeTel;

    private String employeeAddress;

    private String isEnable;

    private LocalDateTime dateJoin;

    private LocalDateTime dateResign;
    private EmployeeItem(EmployeeItemBuilder builder){
        this.id = builder.id;
        this.employeeNum = builder.employeeNum;
        this.employeeName = builder.employeeName;
        this.employeeTel = builder.employeeTel;
        this.employeeAddress = builder.employeeAddress;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateResign = builder.dateResign;
    }
    public static class EmployeeItemBuilder implements CommonModelBuilder<EmployeeItem>{
        @ApiModelProperty(notes = "사원번호시퀀스")
        private final Long id;
        @ApiModelProperty(notes = "사원번호")
        private final String employeeNum;
        @ApiModelProperty(notes = "사원이름")
        private final String employeeName;
        @ApiModelProperty(notes = "사원번호전화번호")
        private final String employeeTel;
        @ApiModelProperty(notes = "사원주소")
        private final String employeeAddress;
        @ApiModelProperty(notes = "사원번호퇴사여부")
        private final String isEnable;
        @ApiModelProperty(notes = "사원입사일")
        private final LocalDateTime dateJoin;
        @ApiModelProperty(notes = "사원퇴사일")
        private final LocalDateTime dateResign;

        public EmployeeItemBuilder(Employee employee){
            this.id = employee.getId();
            this.employeeNum = employee.getEmployeeNum();
            this.employeeName = employee.getEmployeeName();
            this.employeeTel = employee.getEmployeeTel();
            this.employeeAddress = employee.getEmployeeAddress();
            this.isEnable = employee.getIsEnable() ? "예": "아니오";
            this.dateJoin = employee.getDateJoin();
            this.dateResign = employee.getDateResign();
        }
        @Override
        public EmployeeItem build() {
            return new EmployeeItem(this);
        }
    }
}
