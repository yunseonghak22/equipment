package com.example.equipment.model;

import com.example.equipment.entity.Employee;
import com.example.equipment.entity.Equipment;
import com.example.equipment.entity.UsageDetails;
import com.example.equipment.enums.EquipmentType;
import com.example.equipment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem { private Long id;
    @ApiModelProperty(notes = "사용내역시퀀스")
    private Long usageDetailsId;
    @ApiModelProperty(notes = "사용내역일")
    private LocalDateTime dateUsage;
    @ApiModelProperty(notes = "사원번호시퀀스")
    private Long employeeId;
    @ApiModelProperty(notes = "기자재시퀀스")
    private Long equipmentId;
    @ApiModelProperty(notes = "기자재풀이름")
    private String equipmentFullName;
    @ApiModelProperty(notes = "사원번호")
    private String employeeNum;
    @ApiModelProperty(notes = "사원이름")
    private String employeeName;
    @ApiModelProperty(notes = "사원전화번호")
    private String employeeTel;
    @ApiModelProperty(notes = "사원주소")
    private String employeeAddress;
    @ApiModelProperty(notes = "사원퇴사여부")
    private Boolean employeeIsEnable;
    @ApiModelProperty(notes = "사원입사일")
    private LocalDateTime employeeDateJoin;
    @ApiModelProperty(notes = "사원퇴사일")
    private LocalDateTime employeeDateResign;
    private UsageDetailsItem(UsageDetailsItemBuilder builder){
        this.usageDetailsId  = builder.usageDetailsId;
        this.dateUsage = builder.dateUsage;
        this.employeeId = builder.employeeId;
        this.equipmentId = builder.equipmentId;
        this.equipmentFullName = builder.equipmentFullName;
        this.employeeNum = builder.employeeNum;
        this.employeeName = builder.employeeName;
        this.employeeTel = builder.employeeTel;
        this.employeeAddress =builder.employeeAddress;
        this.employeeIsEnable = builder.employeeIsEnable;
        this.employeeDateJoin = builder.employeeDateJoin;
        this.employeeDateResign = builder.employeeDateResign;
    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem>{
        private final Long usageDetailsId;
        private final LocalDateTime dateUsage;
        private final Long employeeId;
        private final Long equipmentId;
        private final String equipmentFullName;
        private final String employeeNum;
        private final String employeeName;
        private final String employeeTel;
        private final String employeeAddress;
        private final Boolean employeeIsEnable;
        private final LocalDateTime employeeDateJoin;
        private final LocalDateTime employeeDateResign;
        public UsageDetailsItemBuilder(UsageDetails usageDetails){
            this.usageDetailsId = usageDetails.getId();
            this.dateUsage = usageDetails.getDateUsage();
            this.employeeId =usageDetails.getEmployee().getId();
            this.equipmentId = usageDetails.getEquipment().getId();
            this.equipmentFullName = usageDetails.getEquipment().getManufacturer()+ " " +usageDetails.getEquipment().getEquipmentType();
            this.employeeNum = usageDetails.getEmployee().getEmployeeNum();
            this.employeeName = usageDetails.getEmployee().getEmployeeName();
            this.employeeTel = usageDetails.getEmployee().getEmployeeTel();
            this.employeeAddress = usageDetails.getEmployee().getEmployeeAddress();
            this.employeeIsEnable =usageDetails.getEmployee().getIsEnable();
            this.employeeDateJoin = usageDetails.getEmployee().getDateJoin();
            this.employeeDateResign = usageDetails.getEmployee().getDateResign();
        }
        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }



}
