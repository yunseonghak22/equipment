package com.example.equipment.model;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommonResult {
    private Boolean isSuccess;  //true, false 값
    private Integer code;
    private String msg;
}
