package com.example.equipment.interfaces;

public interface CommonModelBuilder<T> {  //반복작업을 줄일때 제너릭사용
    T build();   //인터페이스 할일을 정해주고 구현은 니가해

}
