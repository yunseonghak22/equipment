package com.example.equipment.exception;
//출구의 기능을 상속받야야한다. 나만의 출구 전략
//왼쪽 번개 표시가 포인트
public class CMissingDateException extends RuntimeException{
    public CMissingDateException(String msg, Throwable t) {
        super(msg, t);
        //super은 멀끼?  부모변수의 값을 출력
        // super() 메소드는 부모 클래스의 생성자를 호출할 때 사용됩니다.
    }
    public CMissingDateException(String msg){
        super(msg);
    }
    public CMissingDateException(){
        super();
    }
}
