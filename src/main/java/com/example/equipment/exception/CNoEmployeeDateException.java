package com.example.equipment.exception;

public class CNoEmployeeDateException extends RuntimeException{
    public CNoEmployeeDateException(String msg, Throwable t) {
        super(msg, t);
        //super은 멀끼?  부모변수의 값을 출력
        // super() 메소드는 부모 클래스의 생성자를 호출할 때 사용됩니다.
    }
    public CNoEmployeeDateException(String msg){
        super(msg);
    }
    public CNoEmployeeDateException(){
        super();
    }
}
