package com.example.equipment.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter  //enum을 만들고 2개 명찰이 필요하다.
@AllArgsConstructor
public enum ResultCode {
    //비용,시간이 들기 때문에 이넘을 만들어 쓴다.필드와 닭은 꼴이다.
    SUCCESS(0, " 성공하였습니다.")
    ,FAILED(-1, " 실패하였습니다..")

    ,MISSING_DATE(-10000,"데이타을 찾을 수 없습니다.") // 콜센터처럼 규칙을 만들고 있다. 이넘 단어와단어 _로 한다.
    ,No_EMPLOYEE_DATE(-15000,"퇴사하였습니다.")
    ;
    private final Integer code;
    private final String msg;

}
