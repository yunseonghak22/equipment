package com.example.equipment.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EquipmentType{
    COMPUTER("컴퓨터"),
    DESK("책상"),
    CHAIR("의자");

    private final String name;

}
